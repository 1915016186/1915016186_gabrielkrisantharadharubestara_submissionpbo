/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponen;

/**
 *
 * @author Gabriel
 */
public class cpu extends komponen{
    private String nama_cpu,merek;
    private int harga;
    public cpu(String pemilik,String nama_cpu,String merek,int harga){
        super(pemilik);
        this.nama_cpu = nama_cpu;
        this.merek = merek;
        this.harga = harga;  
    }

    public String getNama_cpu() {
        return nama_cpu;
    }

    public void setNama_cpu(String nama_cpu) {
        this.nama_cpu = nama_cpu;
    }

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
    
}
