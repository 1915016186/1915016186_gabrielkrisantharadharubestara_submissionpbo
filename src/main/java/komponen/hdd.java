/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponen;

/**
 *
 * @author Gabriel
 */
public class hdd extends komponen{
    private String nama_hdd,merek;
    int harga;
    public hdd(String pemilik,String nama_hdd,String merek,int harga){
        super(pemilik);
        this.nama_hdd = nama_hdd;
        this.merek = merek;
        this.harga = harga;  
    }

    public String getNama_hdd() {
        return nama_hdd;
    }

    public void setNama_hdd(String nama_hdd) {
        this.nama_hdd = nama_hdd;
    }

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
    
}
